﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace socketClientTest
{
    public class SslTcpClient
    {
        private static Hashtable certificateErrors = new Hashtable();
        private static X509Certificate2Collection certificateCollection;

        public static X509Certificate2Collection CertificateCollection { get => certificateCollection; set => certificateCollection = value; }

        public static X509Certificate2 GetSigningCredential(string thumbprint)
        {

            Console.WriteLine("GetSigningCredential");
            if (thumbprint == null) throw new ArgumentNullException(nameof(thumbprint));

            using (var localMachineCertificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine))
            {
                localMachineCertificateStore.Open(OpenFlags.ReadOnly);
                var certCollection = localMachineCertificateStore.Certificates.Find(
                    X509FindType.FindByThumbprint,
                    thumbprint,
                    false
                );

                CertificateCollection = new X509Certificate2Collection();
                // Get the first cert with the thumbprint
                if (certCollection.Count > 0)
                {
                    CertificateCollection.AddRange(certCollection);
                }
                else
                {
                    throw new Exception(
                        $"The certificate with thumbprint {thumbprint} does not exist in the {StoreName.My} collection of the {StoreLocation.LocalMachine}"
                    );
                }
            }

            return CertificateCollection[0];
        }

        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {

            Console.WriteLine("ValidateServerCertificate Certificate error: {0}", sslPolicyErrors);

            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("ValidateServerCertificate chain.ChainStatus.Length: {0}", chain.ChainStatus.Length);
            if (chain.ChainStatus.Length == 1)
                // Self signed certificates have the issuer in the subject field
                if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors || certificate.Subject == certificate.Issuer)
                {
                    Console.WriteLine("ValidateServerCertificate chain.ChainStatus[0].Status: {0}", chain.ChainStatus[0].Status);
                    // If THIS is the cause of of the error then allow the certificate, a static 0 as the index is safe given chain.ChainStatus.Length == 1.
                    if (chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot)
                    {
                        // Self-signed certificates with an untrusted root are valid.
                        Console.WriteLine("ValidateServerCertificate Self-signed certificates with an untrusted root are valid: {0}", sslPolicyErrors);
                        return true;
                    }
                }
            Console.WriteLine("ValidateServerCertificate Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }
        public static X509Certificate2 SelectUserCertificate(
            object sender,
            string targetHost,
            X509CertificateCollection collection,
            X509Certificate remoteCert,
            string[] acceptableIssuers)
        {
            Console.WriteLine("SelectUserCertificate CertThumbprint : {0}", ConfigurationManager.AppSettings["CertThumbprint"]);
            return GetSigningCredential(ConfigurationManager.AppSettings["CertThumbprint"]);
        }


        public static X509Certificate SelectLocalCertificate(
    object sender,
    string targetHost,
    X509CertificateCollection localCertificates,
    X509Certificate remoteCertificate,
    string[] acceptableIssuers)
        {
            Console.WriteLine("Client is selecting a local certificate.");
            if (acceptableIssuers != null &&
                acceptableIssuers.Length > 0 &&
                localCertificates != null &&
                localCertificates.Count > 0)
            {
                // Use the first certificate that is from an acceptable issuer.
                foreach (X509Certificate certificate in localCertificates)
                {
                    string issuer = certificate.Issuer;
                    if (Array.IndexOf(acceptableIssuers, issuer) != -1)
                        return certificate;
                }
            }
            if (localCertificates != null &&
                localCertificates.Count > 0)
                return localCertificates[0];

            return null;
        }













        public static bool ValidateClientCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            Console.WriteLine("ValidateClientCertificate Certificate error: {0}", sslPolicyErrors);

            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("ValidateClientCertificate chain.ChainStatus.Length: {0}", chain.ChainStatus.Length);
            if (chain.ChainStatus.Length == 1)
                // Self signed certificates have the issuer in the subject field
                if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors || certificate.Subject == certificate.Issuer)
                {
                    Console.WriteLine("ValidateClientCertificate chain.ChainStatus[0].Status: {0}", chain.ChainStatus[0].Status);
                    // If THIS is the cause of of the error then allow the certificate, a static 0 as the index is safe given chain.ChainStatus.Length == 1.
                    if (chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot)
                    {
                        // Self-signed certificates with an untrusted root are valid.
                        Console.WriteLine("ValidateClientCertificate Self-signed certificates with an untrusted root are valid: {0}", sslPolicyErrors);
                        return true;
                    }
                }
            Console.WriteLine("ValidateClientCertificate Certificate error: {0}", sslPolicyErrors);

            return false;
        }

        public static void RunClient(SslStream sslStream, byte[] message)
        {

            // Send hello message to the server.
            sslStream.Write(message);
            sslStream.Flush();
            // Read message from the server.
            string serverMessage = ReadMessage(sslStream);
        }
        static string ReadMessage(SslStream sslStream)
        {
            var messageEndToken = string.Empty + (char)(28) + (char)(13);
            // Read the  message sent by the server.
            // The end of the message is signaled using the
            // "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            int bytes = -1;
            do
            {
                bytes = sslStream.Read(buffer, 0, buffer.Length);

                // Use Decoder class to convert from bytes to UTF8
                // in case a character spans two buffers.
                Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                // Check for EOF.
                if (messageData.ToString().IndexOf(messageEndToken) != -1)
                {
                    break;
                }
            } while (bytes != 0);

            string replacement = Regex.Replace(messageData.ToString(), @"\n", "");
            Console.WriteLine($"{Regex.Replace(replacement, @"\r", "\n")}");
            return messageData.ToString();
        }
    }
}
