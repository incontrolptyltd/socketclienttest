﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace socketClientTest
{

    public class CustomEndPoint
    {
        private readonly int _instance;
        private readonly HttpClient _client;
        private readonly string _appserver;
        private readonly string _endpoint;

        public CustomEndPoint(int instance)
        {
            _instance = instance;
            _client = new HttpClient();
            _appserver = ConfigurationManager.AppSettings["carpswsurl"];
            _endpoint = ConfigurationManager.AppSettings["endpoint"];

            if (!string.IsNullOrEmpty(_appserver))
            {
                _client.BaseAddress = new Uri(_appserver);
            }
            else
            {
                throw new ArgumentNullException("Invalid web service url");
            }

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task CustomPostToClient(List<List<Object>> allMessages)
        {
            try
            {
                for (int i = 0; i < allMessages.Count; i++)
                {
                    var objects = allMessages[i];
                    for (int j = 0; j < objects.Count; j++)
                    {
                        HttpResponseMessage response = await _client.PostAsJsonAsync(_endpoint, objects[j]);
                        Console.WriteLine($"{_instance}-{j} => {response}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }

    // State object for receiving data from remote device.  
    public class StateObject
    {
        // Client socket.  
        public Socket workSocket = null;
        // Client  socket.  
        public TcpClient client = null;
        public SslStream sslStream = null;
        public NetworkStream stream = null;
        // Size of receive buffer.  
        public const int BufferSize = 0x8000;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }

    public class AsyncClient
    {
        // The port number for the remote device.  
        private readonly int _port;
        private readonly string[] _data;
        // public ManualResetEvent _doneEvent;

        // ManualResetEvent instances signal completion.  
        private readonly ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private readonly ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private readonly ManualResetEvent receiveDone =
            new ManualResetEvent(false);
        public AsyncClient(int port, string[] data)
        {
            _port = port;
            _data = data;
        }
        // The response from the remote device.  
        private String response = String.Empty;

        public void StartClient()
        {
            // Connect to a remote device.  
            try
            {
                var address = ConfigurationManager.AppSettings["ipaddress"];
                IPAddress ipAddress;
                if (string.IsNullOrEmpty(address))
                {
                    ipAddress = Array.FindLast(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
                } else
                {
                    ipAddress = Array.FindLast(Dns.GetHostEntry(address).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
                }

                var tcpPort = _port;
                var localEndPoint = new IPEndPoint(ipAddress, tcpPort);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, _port);
                

                var tls = ConfigurationManager.AppSettings["UseTls"];

                if (!String.IsNullOrEmpty(tls) && tls.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Create a TCP/IP socket.  
                    Socket client = new Socket(ipAddress.AddressFamily,
                        SocketType.Stream, ProtocolType.Tcp);

                    SentToClientSync(remoteEP, client);

                }
                else
                {

                    SendToServerTLS(remoteEP);
                    //SentToClientSync(remoteEP, client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void SendToServerTLS(IPEndPoint remoteEP)
        {
            var clientCertificateRequired = false;
            var targetHost = ConfigurationManager.AppSettings["TargetHost"];
            var ccr = ConfigurationManager.AppSettings["ClientCertificateRequired"];
            if (!String.IsNullOrEmpty(ccr))
            {
                bool.TryParse(ccr, out clientCertificateRequired);
            }

            // Create a TCP/IP client socket.
            // machineName is the host running the server application.
            TcpClient client = new TcpClient(remoteEP.Address.ToString(), remoteEP.Port);
            Console.WriteLine("Client connected.");

            SslStream sslStream;

            if (clientCertificateRequired)
            {
                Console.WriteLine("clientCertificateRequired");

                sslStream = new SslStream(
                client.GetStream(),
                clientCertificateRequired,
                new RemoteCertificateValidationCallback(SslTcpClient.ValidateClientCertificate),
                new LocalCertificateSelectionCallback(SslTcpClient.SelectUserCertificate));
            } else
            {
               sslStream = new SslStream(
               client.GetStream(),
               clientCertificateRequired,
               new RemoteCertificateValidationCallback(SslTcpClient.ValidateServerCertificate));
            }
            

            // The server name must match the name on the server certificate.
            try
            {
                Console.WriteLine("targetHost" + targetHost);
                sslStream.AuthenticateAsClient(targetHost);
                for (int i = 0; i < _data.Length; i++)
                {
                    Console.WriteLine("send message " + i);
                    // Send the data through the socket.  
                    byte[] msg = Encoding.ASCII.GetBytes(_data[i]);
                    SslTcpClient.RunClient(sslStream, msg);
                    Console.WriteLine("-------------------------------------------------------");
                }
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("client AuthenticationException Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                sslStream.Close();
                client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("client Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                sslStream.Close();
                client.Close();
            }
            finally
            {
                sslStream.Close();
                client.Close();
            }
        }


        private void SentToClientSync(IPEndPoint remoteEP, Socket client)
        {
            byte[] bytes = new byte[8192];

            client.Connect(remoteEP);
            for (int i = 0; i < _data.Length; i++)
            {
                // Send the data through the socket.  
                byte[] msg = Encoding.ASCII.GetBytes(_data[i]);
                _ = client.Send(msg);

                // Receive the response from the remote device.  
                int bytesRec = client.Receive(bytes);

                var response = Encoding.ASCII.GetString(bytes, 1, bytesRec - 1);
                string replacement = Regex.Replace(response, @"\n", "");
                Console.WriteLine($"{Regex.Replace(replacement, @"\r", "\n")}");
                Console.WriteLine("------------------------------------------------------");
            }

            client.Close();

        }


        private void SendToClient(IPEndPoint remoteEP, Socket client)
        {
            // Connect to the remote endpoint.  
            client.BeginConnect(remoteEP,
                new AsyncCallback(ConnectCallback), client);
            connectDone.WaitOne();

            for (int i = 0; i < _data.Length; i++)
            {
                Send(client, _data[i]);
                sendDone.WaitOne();

                // Receive the response from the remote device.  
                Receive(client);
                receiveDone.WaitOne();

                // Write the response to the console.  
                Console.WriteLine("Response received {1}: {0}", response, response.Length);
                // Thread.Sleep(50);
            }

        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.  
                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void Receive(Socket client)
        {
            try
            {
                // Create the state object.  
                StateObject state = new StateObject
                {
                    workSocket = client
                };

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                    Console.WriteLine(state.sb.ToString());

                    if (state.sb.ToString().IndexOf((char)(28)) > 1)
                    {
                        receiveDone.Set();
                    }
                    else
                    {
                        // Get the rest of the data.  
                        client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                    }

                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static X509Certificate2 GetSigningCredential(string thumbprint)
        {
            if (thumbprint == null) throw new ArgumentNullException(nameof(thumbprint));

            var cert = default(X509Certificate2);

            using (var localMachineCertificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine))
            {
                localMachineCertificateStore.Open(OpenFlags.ReadOnly);
                var certCollection = localMachineCertificateStore.Certificates.Find(
                    X509FindType.FindByThumbprint,
                    thumbprint,
                    false
                );

                // Get the first cert with the thumbprint
                if (certCollection.Count > 0)
                {
                    cert = certCollection[0];
                }
                else
                {
                    throw new Exception(
                        $"The certificate with thumbprint {thumbprint} does not exist in the {StoreName.My} collection of the {StoreLocation.LocalMachine}"
                    );
                }
            }

            return cert;
        }
    }

    public class Program
    {
        private static readonly ManualResetEvent _doneEvent = new ManualResetEvent(false);
        public static readonly ManualResetEvent _allDone = new ManualResetEvent(false);

        private static int _numberOfThreadsNotYetCompleted = 1;
        private static string _messageEndToken;
        static readonly HttpClient client = new HttpClient();

        public static int TcpPort { get; private set; }
        public static TcpListener Listener { get; private set; }

        public static int Main()
        {
            Console.WriteLine(Assembly.GetExecutingAssembly().GetName().Version.Major + "." + Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + Assembly.GetExecutingAssembly().GetName().Version.Build + "." + Assembly.GetExecutingAssembly().GetName().Version.MinorRevision);
            var method = ConfigurationManager.AppSettings["method"];
            var inputs = ConfigurationManager.AppSettings["inputs"];
            var processType = ConfigurationManager.AppSettings["processType"];

            if (processType.Equals("custom", StringComparison.InvariantCultureIgnoreCase) && method.Equals("web", StringComparison.InvariantCultureIgnoreCase))
            {
                HitEndpoint(inputs);
            }
            else
            {
                string s = ConfigurationManager.AppSettings["MessageEndToken"];
                if (!String.IsNullOrEmpty(s))
                {
                    _messageEndToken = s;
                }
                else
                {
                    _messageEndToken = string.Empty + (char)(28) + (char)(13);
                }

                if (processType.Equals("radios", StringComparison.InvariantCultureIgnoreCase))
                {
                    RadioServer();
                }
                else
                {
                    List<List<String>> allMessages = new List<List<string>>();
                    if (!string.IsNullOrEmpty(inputs))
                    {
                        foreach (string input in inputs.Split(','))
                        {
                            allMessages.Add(ReadMessages(input));
                        }

                        ProcessMessages(processType, method, allMessages);

                    }
                    else
                    {
                        throw new ArgumentNullException("No inputs found");
                    }
                }
            }
            return 0;
        }

        public static void RadioServer()
        {
            var address = ConfigurationManager.AppSettings["ipaddress"];
            IPAddress ipAddress;
            if (string.IsNullOrEmpty(address))
            {
                ipAddress = Array.FindLast(Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
            }
            else
            {
                ipAddress = Array.FindLast(Dns.GetHostEntry(address).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
            }

            var socketPort = ConfigurationManager.AppSettings["socketPort"];

            if (string.IsNullOrEmpty(socketPort))
            {
                throw new ArgumentNullException("Missing socket port");
            }

            try
            {

                // Establish the local endpoint for the socket.  
                TcpPort = Convert.ToInt32(socketPort);
                var localEndPoint = new IPEndPoint(ipAddress, TcpPort);

                // Create a TCP/IP socket.  
                Listener = new TcpListener(localEndPoint);

                Console.WriteLine("Starting TCP Listen :" + ipAddress + ":" + TcpPort + "" + Environment.NewLine + "Date :" + DateTime.Now,  Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);

                // Bind the socket to the local endpoint and listen for incoming connections.  
                try
                {
                    Listener.Start();

                    while (!_doneEvent.WaitOne(0))
                    {
                        // Set the event to non signaled state.  
                        _allDone.Reset();

                        // Start an asynchronous socket to listen for connections.  
                        Console.WriteLine("Waiting for a connection...");

                        Listener.BeginAcceptTcpClient(
                            new AsyncCallback(AcceptCallback),
                            Listener);

                        // Wait until a connection is made before continuing.  
                        _allDone.WaitOne();
                        Console.WriteLine("Client Connected...");
                    }

                    _doneEvent.Set();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                Console.WriteLine("\nAny key to Stop Listening...");
                Console.Read();
                _doneEvent.Set();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Get the socket that handles the client request.  
            // Get the listener that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;

            // End the operation and display the received data on
            // the console.
            TcpClient client = listener.EndAcceptTcpClient(ar);

            // Create the state object.  
            StateObject state = new StateObject
            {
                client = client,
                stream = client.GetStream(),
            };

            try
            {
                state.stream.BeginRead(state.buffer, 0, StateObject.BufferSize, new AsyncCallback(ReadCallback), state);
            }
            catch (Exception ex)
            {
                state.stream.Close();
                client.Close();
                listener.Stop();
            }

            // Signal the main thread to continue.  
            _allDone.Set();
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            TcpClient client = state.client;

            try
            {
                // Read data from the client socket.   
                int bytesRead = state.stream.EndRead(ar);

                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.  
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read   
                    // more data.  
                    content = state.sb.ToString();
                    Console.WriteLine("Received: {0}", content);

                    // wait for next message
                    state.sb.Clear();
                    state.stream.BeginRead(state.buffer, 0, StateObject.BufferSize, new AsyncCallback(ReadCallback), state);
                }
                else
                {
                    Console.WriteLine($"Zero Bytes Received, Client Disconnected.");
                    state.stream.Close();
                    state.client.Close();
                }
            }
            catch (Exception ex)
            {
                state.stream.Close();
                client.Close();
            }

        }

        private static void ProcessMessages(string processType, string method, List<List<string>> allMessages)
        {
            _numberOfThreadsNotYetCompleted = allMessages.Count();
            if (method.Equals("socket", StringComparison.InvariantCultureIgnoreCase))
            {
                ViaCarpsHL7(allMessages);
            }
            else
            {
                if (method.Equals("web", StringComparison.InvariantCultureIgnoreCase))
                {
                    RunAsync(allMessages).GetAwaiter().GetResult();
                }
            }
        }

        private static void HitEndpoint(string inputs)
        {
            var endpoint = ConfigurationManager.AppSettings["endpoint"];
            List<List<Object>> allMessages = new List<List<Object>>();
            if (!string.IsNullOrEmpty(inputs))
            {
                foreach (string input in inputs.Split(','))
                {
                    allMessages.Add(ReadJson(input));
                }

                var repeat = Convert.ToInt32(ConfigurationManager.AppSettings["supportRepeat"]);
                var tasks = new List<Task>();

                for (int i = 0; i < repeat; i++)
                {
                    var custom = new CustomEndPoint(i + 1);
                    tasks.Add(Task.Run(async () =>
                    {
                        await custom.CustomPostToClient(allMessages);
                    }));
                }
                Task t = Task.WhenAll(tasks);
                try
                {
                    t.Wait();
                }
                catch { }

                Console.WriteLine("Hit Enter to exit...");
                Console.ReadLine();
            }
            else
            {
                throw new ArgumentNullException("No inputs found");
            }
        }

        private static List<Object> ReadJson(string v)
        {
            List<Object> items = new List<Object>();
            using (StreamReader r = new StreamReader(v))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<Object>>(json);
            }

            return items;
        }

        private static List<String> ReadMessages(string v)
        {
            string firstline = "First Line\r";
            string line;
            List<string> messages = new List<string>();

            System.IO.StreamReader file =
                new System.IO.StreamReader(v);

            StringBuilder myStringBuilder = new StringBuilder(firstline);

            while ((line = file.ReadLine()) != null)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }
                if (line.StartsWith("MSH|"))
                {
                    messages.Add(myStringBuilder.ToString());
                    myStringBuilder = new StringBuilder(line + "\r");
                }
                else
                {
                    if (line.Length > 4) // e.g "MSH|
                    {
                        myStringBuilder.Append(line + "\r");
                    }
                }
            }

            messages.Add(myStringBuilder.ToString());
            messages.Remove(firstline);
            file.Close();
            return messages;
        }

        private static void ViaCarpsHL7(List<List<string>> allMessages)
        {
            var socketPort = ConfigurationManager.AppSettings["socketPort"];

            if (string.IsNullOrEmpty(socketPort))
            {
                throw new ArgumentNullException("Missing socket port");
            }

            for (int i = 0; i < allMessages.Count; i++)
            {
                List<string> ls = new List<string>();
                List<string> workList = ListSort(allMessages[i]);

                foreach (var message in workList)
                {
                    string replacement = (char)(11) + Regex.Replace(message, @"\n", "") + _messageEndToken;
                    ls.Add(replacement);
                }

                string[] data = ls.ToArray();
                AsyncClient ac1 = new AsyncClient(Convert.ToInt32(socketPort), data);
                ThreadPool.QueueUserWorkItem(new WaitCallback(DoWork), ac1);
            }

            // Wait for all threads in pool to calculation...
            Console.WriteLine("All calculations are complete.");
            Console.WriteLine("Press any key to stop...");
            Console.ReadKey(true);
        }

        private static List<string> ListSort(List<string> ls)
        {
            List<string> workList = new List<string>();
            var sort = ConfigurationManager.AppSettings["sort"];
            if (!String.IsNullOrEmpty(sort) && sort.Equals("true", StringComparison.InvariantCultureIgnoreCase))
            {
                int[] startEnd = GetDateStartEnd(ls.First());
                workList = ls.OrderBy(o => o.Substring(startEnd[0], startEnd[1])).ToList();
            }
            else
            {
                workList = ls;
            }

            return workList;
        }

        static async Task RunAsync(List<List<string>> allMessages)
        {
            var carpswsurl = ConfigurationManager.AppSettings["carpswsurl"];
            if (!string.IsNullOrEmpty(carpswsurl))
            {
                client.BaseAddress = new Uri(carpswsurl);
            }
            else
            {
                throw new ArgumentNullException("Invalid web service url");
            }

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("text/plain"));

            try
            {
                for (int i = 0; i < allMessages.Count; i++)
                {
                    List<string> workList = ListSort(allMessages[i]);
                    for (int J = 0; J < 1; J++)
                    {
                        foreach (var message in workList)
                        {
                            string replacement = Regex.Replace(message, @"\n", "");
                            var ack = await CreateHL7Async(replacement);
                            Console.WriteLine($"{Regex.Replace(ack, @"\r", "\n")}");
                            Console.WriteLine("------------------------------------------------------");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Hit Enter to exit...");
            Console.ReadLine();
        }

        


        public static void DoWork(object data)
        {
            AsyncClient ac = (AsyncClient)data;
            try
            {
                ac.StartClient();
                _doneEvent.Set();
            }
            finally
            {
                if (Interlocked.Decrement(ref _numberOfThreadsNotYetCompleted) == 0)
                    Console.WriteLine("Client completed");
            }

        }        

        static async Task<string> CreateHL7Async(string hl7Message)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress + "HL7Integration/V1.0/PatientData/PatientDataIntegration");
            request.Content = new StringContent(hl7Message, Encoding.UTF8, "text/plain");//CONTENT-TYPE header
            
            var response = await client.SendAsync(request);

            response.EnsureSuccessStatusCode();
            var ack = await response.Content.ReadAsStringAsync();
            return ack;
        }

         public static int[] GetDateStartEnd(string hl7Message)
        {
            int[] startEnd = new int[2];
            var Message = new HL7Models.HL7Message(hl7Message);
            var MessageSections = hl7Message.Split(new[] { "\r" }, StringSplitOptions.RemoveEmptyEntries);
            var HeaderSubString = MessageSections[0].Split(Message.Hl7Header.EnodingCharacters[0]);
            startEnd[0] = hl7Message.IndexOf(HeaderSubString[6]);
            startEnd[1] = HeaderSubString[6].Length;
            return startEnd;
        }

        
    }
}