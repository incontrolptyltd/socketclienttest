﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace socketClientTest
{
    internal class HL7Models
    {
        public class HL7Message
        {
            public HL7Header Hl7Header { get; set; }
            public HL7Event Hl7Event { get; set; }
            public HL7PatientID Hl7PatientId { get; set; }
            public HL7PatientVisit Hl7PatientVisit { get; set; }

            public HL7Message()
            {
                Hl7Header = new HL7Header();
                Hl7Event = new HL7Event();
                Hl7PatientId = new HL7PatientID();
                Hl7PatientVisit = new HL7PatientVisit();
            }

            public HL7Message(string hl7Message)
            {
                Hl7Header = new HL7Header(hl7Message);
                Hl7Event = new HL7Event();
                Hl7PatientId = new HL7PatientID();
                Hl7PatientVisit = new HL7PatientVisit();
            }
        }
        public class HL7Header
        {
            public char[] EnodingCharacters { get; set; }
            public string SendingApplication { get; set; }
            public string SendingFacility { get; set; }
            public string ReceivingApplication { get; set; }
            public string ReceivingFacility { get; set; }
            public string DateTimeMessage { get; set; }
            public string Security { get; set; }
            public string MessageType { get; set; }
            public string MessageControlID { get; set; }
            public string ProcessingID { get; set; }
            public string VersionID { get; set; }
            public string SequenceNumber { get; set; }
            public string ContinuationPointer { get; set; }
            public string AcceptAcknowledgmentType { get; set; }
            public string ApplicationAcknowledgmentType { get; set; }
            public string CountryCode { get; set; }

            public HL7Header()
            {
                EnodingCharacters = new char[4];
            }

            public HL7Header(string hl7Message)
            {
                EnodingCharacters = new[] { hl7Message[3], hl7Message[4], Convert.ToChar(hl7Message[5]), Convert.ToChar(hl7Message[6]), Convert.ToChar(hl7Message[7]) };
            }
        }

        public class HL7Event
        {
            public string EventTypeCode { get; set; }
            public string RecordedDateTime { get; set; }
            public string DateTimePlannedEvent { get; set; }
            public string EventReasonCode { get; set; }
            public string OperatorID { get; set; }
            public string EventOccurred { get; set; }
            public string EventFacility { get; set; }
        }

        public class HL7PatientID
        {
            public string PIDSetID { get; set; }
            public string PatientID { get; set; }
            public string[] PatientIDList { get; set; }
            public string AlternatePatientID { get; set; }
            public string[] PatientName { get; set; }
            public string MothersMaidenName { get; set; }
            public string DateTimeOfBirth { get; set; }
            public string AdministrativeSex { get; set; }
            public string PatientAlias { get; set; }
            public string Race { get; set; }
            public string PatientAddress { get; set; }
            public string CountryCode { get; set; }
            public string HomePhoneNumber { get; set; }
            public string BusinessPhoneNumber { get; set; }
            public string Language { get; set; }
            public string MaritalStatus { get; set; }
            public string Religion { get; set; }
            public string PatientAccountNumber { get; set; }
            public string SSNNumber { get; set; }
            public string DriversLicenseNumber { get; set; }
            public string MothersIdentifier { get; set; }
            public string EthnicGroup { get; set; }
            public string BirthPlace { get; set; }
            public string MulitpleBirthIndicator { get; set; }
            public string BirthOrder { get; set; }
            public string Citizenship { get; set; }
            public string VeteransStatus { get; set; }
            public string Nationality { get; set; }
            public string PatientDeathDateTime { get; set; }
            public string PatientDeathIndicator { get; set; }
            public string IdentityUnknownIndicator { get; set; }
            public string IdentityReliabilityCode { get; set; }
            public string LastUpdateDateTime { get; set; }
            public string LastUpdateFacility { get; set; }
            public string SpeciesCode { get; set; }
            public string BreedCode { get; set; }
            public string Stain { get; set; }
            public string ProductionClassCode { get; set; }
            public string TribalCitizenship { get; set; }
            public string PatientTelecommunicationInformation { get; set; }
        }

        public class HL7PatientVisit
        {
            public string PV1SetID { get; set; }
            public string PatientClass { get; set; }
            public string[] AssignedPatientLocation { get; set; }
            public string AdmissionType { get; set; }
            public string PreadmitNumber { get; set; }
            public string PriorPatientLocation { get; set; }
            public string AttendingDoctor { get; set; }
            public string ReferringDoctor { get; set; }
            public string ConsultingDoctor { get; set; }
            public string HospitalService { get; set; }
            public string TemporaryLocation { get; set; }
            public string PreadmitTestIndicator { get; set; }
            public string ReadmissionIndicator { get; set; }
            public string AdmitSource { get; set; }
            public string AmbulatoryStatus { get; set; }
            public string VIPIndicator { get; set; }
            public string AdmittingDoctor { get; set; }
            public string PatientType { get; set; }
            public string VisitNumber { get; set; }
            public string FinancialClass { get; set; }
            public string ChargePriceIndicator { get; set; }
            public string CourtesyCode { get; set; }
            public string CreditRating { get; set; }
            public string ContractCode { get; set; }
            public string ContractEffectiveDate { get; set; }
            public string ContractAmount { get; set; }
            public string ContractPeriod { get; set; }
            public string InterestCode { get; set; }
            public string TransferToBadDebtCode { get; set; }
            public string TransferToBadDebtDate { get; set; }
            public string BadDebtAgencyCode { get; set; }
            public string BadDebtTransferAmount { get; set; }
            public string BadDebtRecoveryAmount { get; set; }
            public string DeleteAccountIndicator { get; set; }
            public string DeleteAccountDate { get; set; }
            public string DischargeDisposition { get; set; }
            public string DischargedToLocation { get; set; }
            public string DietType { get; set; }
            public string ServicingFacility { get; set; }
            public string BedStatus { get; set; }
            public string AccountStatus { get; set; }
            public string PendingLocaiton { get; set; }
            public string PriorTemporaryLocation { get; set; }
            public string AdmitDateTime { get; set; }
            public string DischargeDateTime { get; set; }
            public string CurrentPatientBalance { get; set; }
            public string TotalCharges { get; set; }
            public string TotalAdjustments { get; set; }
            public string TotalPayments { get; set; }
            public string AlternateVisitID { get; set; }
            public string VisitIndicator { get; set; }
            public string OtherHealthCareProvider { get; set; }
            public string ServiceEpisodeDescription { get; set; }
            public string ServiceEpisodeIdentifier { get; set; }
        }

        public class HL7PatientVisitAdditionalInformation
        {
            public string PriorPendingLocation { get; set; }
            public string AccommodationCode { get; set; }
            public string AdmitReason { get; set; }
            public string TransferReason { get; set; }
            public string PatientValuables { get; set; }
            public string PatientValuablesLocation { get; set; }
            public string VisitUserCode { get; set; }
            public string ExpectedAdmitDateTime { get; set; }
            public string ExpectedDischargeDateTime { get; set; }
            public string EstimatedLengthOfInpatientStay { get; set; }
            public string AcutalLengthOfInpatientStay { get; set; }
            public string VisitDescription { get; set; }
            public string ReferralSourceCode { get; set; }
            public string PreviousServiceDate { get; set; }
            public string EmploymentIllnessRelatedIndicator { get; set; }
            public string PurgeStatusCode { get; set; }
            public string PurgeStatusDate { get; set; }
            public string SpecialProgramCode { get; set; }
            public string RetentionIndicator { get; set; }
            public string ExpectedNumberofInsurancePlans { get; set; }
            public string VisitPublicityCode { get; set; }
            public string VisitProtectionIndicator { get; set; }
            public string ClinicOrganizationname { get; set; }
            public string PatientStatusCode { get; set; }
            public string VisitPriorityCode { get; set; }
            public string PreviousTreatmentDate { get; set; }
            public string ExpectedDischargeDispotition { get; set; }
            public string SignatureOnFileData { get; set; }
            public string FirstSimilarIllnessDate { get; set; }
            public string PatientChargeAdjustmentCode { get; set; }
            public string RecurringServiceCode { get; set; }
            public string BillingMediaCode { get; set; }
            public string ExpectedSurgeryDateTime { get; set; }
            public string MilitaryPartnershipCode { get; set; }
            public string MilitaryNonAvailabilityCode { get; set; }
            public string NewbornBabyIndicator { get; set; }
            public string BabyDetainedIndicator { get; set; }
            public string ModeOfArriveCode { get; set; }
            public string RecreationalDrugUseCode { get; set; }
            public string AdmissionLevelOfCareCode { get; set; }
            public string PrecautionCode { get; set; }
            public string PatientConditionCode { get; set; }
            public string LivingWillCode { get; set; }
            public string OrganDonorCode { get; set; }
            public string AdvanceDirectiveCode { get; set; }
            public string PatientStatusEffectiveDate { get; set; }
            public string ExpectedLOAReturnDateTime { get; set; }
            public string ExpectedPreAdmissionTestingDateTime { get; set; }
            public string NotifyClergyCode { get; set; }
            public string AdvanceDirectiveLastVerifiedDate { get; set; }
        }

        public class HL7AcessRestriction
        {
            public string SetID { get; set; }
            public string AccessRestrictionActionCode { get; set; }
            public string AccessRestrictionValue { get; set; }
            public string AccessRestrictionReason { get; set; }
            public string SpecialAccessRestrictionInstructions { get; set; }
            public string AccessResctrictionDateRange { get; set; }
        }
    }
}
